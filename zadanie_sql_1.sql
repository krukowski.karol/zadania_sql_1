CREATE TABLE Pracownicy (
   ID INTEGER NOT NULL, 
   Imie TEXT,
   Nazwisko TEXT,
   Wiek INTEGER,
   Kurs TEXT
);

INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (1, "Anna", "NOWAK", 34, "DS.");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (2, "Roman", "Kowalski", 42, "DS.");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (3, "Tomasz", "WIŚNIEWSKI",	33,	"DS.");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (4, "Agata", "WÓJCIK", 43, "DS.");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (5, "Elżbieta", "KOWALCZYK", 28, "Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (6, "Przemysław", "KAMIŃSKI", 34, "Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (7, "Robert", "LEWANDOWSKI", 35, "Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (8, "Radosław",	"ZIELIŃSKI", 38, "Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (9, "Anna",	"WOŹNIAK", 26, "Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (10, "Robert", "SZYMAŃSKI",	34,	"Java");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (11, "Radosław", "DĄBROWSKI", 35, "UX");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (12, "Robert", "KOZŁOWSKI",	38,	"UX");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (13, "Joanna", "MAZUR",	26,	"UX");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (14, "Radosław", "JANKOWSKI", 27, "UX");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (15, "Patryk","LEWANDOWSKI", 28, "Tester");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (16, "Patryk", "ZIELIŃSKI", 28, "Tester");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (17, "Andrzej",	"WOŹNIAK", 31, "Tester");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (18, "Andrze", "LEWANDOWSKI", 30, "Tester");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (19, "Roman", "ZIELIŃSKI", 39, "Tester");
INSERT INTO Pracownicy (ID, Imie, Nazwisko, Wiek, Kurs
) 
VALUES (20, "Ewa", "WOŹNIAK", 31, "Tester");



SELECT Wiek
FROM Pracownik
WHERE Wiek > 30;
